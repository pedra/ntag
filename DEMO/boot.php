<?php

// autoloader
spl_autoload_register(
    function ($class) {
        $file = str_replace('\\', '/', __DIR__.'/'.$class.'.php');
        if (file_exists($file)) {
            require $file;
        }
    }
);


// instantiating class
$nt = new Pedra\NTag;

// add styles & scripts
$nt->setStyle('style.css')
   ->setScript('main.js')


// uncomment the next line and test :)
// ->var('flash', 'Hello World!')


// rendering and sending to the client
   ->render('body')
   ->send();
