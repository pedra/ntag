<?php
/**
 * Config\NTag
 * PHP version 7
 *
 * @category  Template
 * @package   Config
 * @author    Bill Rocha <prbr@ymail.com>
 * @copyright 2018 Bill Rocha <http://google.com/+BillRocha>
 * @license   <https://opensource.org/licenses/MIT> MIT
 * @version   GIT: 0.0.1
 * @link      Site <https://phatto.ga/ntag>
 */

namespace Config\Pedra;

/**
 * Config\NTag Class
 *
 * @category Template
 * @package  Config
 * @author   Bill Rocha <prbr@ymail.com>
 * @license  <https://opensource.org/licenses/MIT> MIT
 * @link     Site <https://phatto.ga/ntag>
 */

class NTag
{
    public static $name      = 'default';
    public static $mode      = 'pro'; //pro|dev
    public static $cacheTime = 0; // 6 hours of life

    // Paths
    public static $template  = '';
    public static $cache     = '';
    public static $style     = '';
    public static $script    = '';

    // HTML parts
    public static $header    = null;
    public static $footer    = null;

    // External
    public static $plugins   = [];

    /**
     * Boot settings
     */
    public function __construct()
    {
        $root               = dirname(dirname(__DIR__));
        
        static::$template   = $root.'/.html';
        static::$cache      = $root.'/.html/.cache';
        static::$mode       = 'pro';

        static::$style      = $root.'/public/css';
        static::$script     = $root.'/public/js';

        static::$header     = static::$template.'/header.html';
        static::$footer     = static::$template.'/footer.html';

        static::$plugins['datacontent'] = '\Plugin\Datacontent';
    }
}
